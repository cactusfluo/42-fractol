/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_precise.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:36:32 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 15:36:33 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw_point(t_fractol *fct, t_point *p)
{
	*(int32_t*)(&(fct->mlx.canvas[p->screen_y * WINDOW_W * 4 + p->screen_x
	* 4])) = get_color(p->level);
}

static void		draw_white(t_fractol *fct, t_point *p)
{
	static int32_t	white = 255 << 16 | 255 << 8 | 255;

	*(int32_t*)(&(fct->mlx.canvas[p->screen_y * WINDOW_W * 4 + p->screen_x
	* 4])) = white;
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_precise(t_fractol *fct)
{
	uint32_t	i;
	uint32_t	to_draw;
	t_point		*p;
	double		domain;

	domain = fct->domain;
	i = 0;
	to_draw = 0;
	while (i < fct->to_draw)
	{
		p = &(fct->points[to_draw]);
		ct_memcpy(p, &(fct->points[i]), sizeof(t_point));
		if (fct->f_precise(fct, p))
			draw_point(fct, p);
		else
		{
			++to_draw;
			draw_white(fct, p);
		}
		++i;
	}
	fct->to_draw = to_draw;
}
