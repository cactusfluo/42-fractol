/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:36:23 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 15:48:03 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw_point(t_fractol *fct, t_point *p)
{
	*(int32_t*)(&(fct->mlx.canvas[p->screen_y * WINDOW_W * 4 + p->screen_x
	* 4])) = get_color(p->level);
}

static void		draw_white(t_fractol *fct, t_point *p)
{
	static int32_t	white = 255 << 16 | 255 << 8 | 255;

	*(int32_t*)(&(fct->mlx.canvas[p->screen_y * WINDOW_W * 4 + p->screen_x
	* 4])) = white;
}

static void		calcul(t_fractol *fct, t_point *p)
{
	if (fct->f_init(fct, p))
	{
		++(fct->to_draw);
		draw_white(fct, p);
	}
	else
		draw_point(fct, p);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_init(t_fractol *fct)
{
	uint32_t	x;
	uint32_t	y;
	t_point		*p;
	double		domain;

	domain = fct->domain;
	y = 0;
	while (y < WINDOW_H)
	{
		x = 0;
		while (x < WINDOW_W)
		{
			p = &(fct->points[fct->to_draw]);
			p->screen_y = y;
			p->y = fct->pos_y + ((double)y / WINDOW_H) * domain - domain / 2;
			p->screen_x = x;
			p->x = fct->pos_x + ((double)x / WINDOW_W) * domain - domain / 2;
			p->zc = p->y;
			p->zr = p->x;
			calcul(fct, p);
			++x;
		}
		++y;
	}
}
