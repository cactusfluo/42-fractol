/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_init_ship.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:36:10 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 15:36:11 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

uint32_t		calcul_init_ship(t_fractol *fct, t_point *point)
{
	double		nzc;
	double		nzr;
	uint32_t	i;

	i = 0;
	point->zc = (double)fct->mouse_y / 400;
	point->zr = (double)fct->mouse_x / 400;
	while (i < MIN_PRECISION)
	{
		nzc = 2 * ABS(point->zc) * ABS(point->zr) + point->y;
		nzr = ABS(point->zr) * ABS(point->zr) - ABS(point->zc) * ABS(point->zc)
		+ point->x;
		point->zc = nzc;
		point->zr = nzr;
		if (point->zc * point->zc + point->zr * point->zr > 4)
			break ;
		++i;
	}
	point->level = i;
	return (i == MIN_PRECISION);
}
