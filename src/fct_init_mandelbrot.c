/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_init_mandelbrot.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:36:46 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 15:36:47 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

uint32_t		calcul_init_mandelbrot(t_fractol *fct, t_point *point)
{
	double		nzc;
	double		nzr;
	uint32_t	i;

	i = 0;
	point->zc = (double)fct->mouse_y / 400;
	point->zr = (double)fct->mouse_x / 400;
	while (i < MIN_PRECISION)
	{
		nzc = 2 * point->zc * point->zr + point->y;
		nzr = point->zr * point->zr - point->zc * point->zc + point->x;
		point->zc = nzc;
		point->zr = nzr;
		if (point->zc * point->zc + point->zr * point->zr > 4)
			break ;
		++i;
	}
	point->level = i;
	return (i == MIN_PRECISION);
}
