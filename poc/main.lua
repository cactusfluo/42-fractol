--[[
--------------------------------------------------------------------------------
--	by REEZ
--	project name: Fractol
--	file name: main.lua
--------------------------------------------------------------------------------
--	description:
--		Main file of Fractol.
--------------------------------------------------------------------------------
--]]


--------------------------------------------------------------------------------
--[ LOVE ]----------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------
--[ LOAD ]----------------------------------------
--------------------------------------------------

function love.load()
	require( "sources.init" )
	m_lib.fps.set_delay( 0.1 )
end

--------------------------------------------------
--[ UPDATE ]--------------------------------------
--------------------------------------------------

function love.update( dt )
	local x_clic
	local y_clic

	m_lib.fps.update( dt )
	fractal:send( 'max_iter', max_iter )
	fractal:send( 'domain', domain )
	fractal:send( 'pos', { pos_x, pos_y } )
	fractal:send( 'mouse', { mouse_x, mouse_y } )
	if (key_l == 1) then pos_x = pos_x - 1 * domain / 20 end
	if (key_r == 1) then pos_x = pos_x + 1 * domain / 20 end
	if (key_u == 1) then pos_y = pos_y - 1 * domain / 20 end
	if (key_d == 1) then pos_y = pos_y + 1 * domain / 20 end
	if (key_zoom == 1) then
		domain = domain * ZOOM_COEF
		x_clic = pos_x + 0.5 * domain - domain / 2
		y_clic = pos_y + 0.5 * domain - domain / 2
		pos_x = x_clic + 0.5 * domain - domain / 2
		pos_y = y_clic + 0.5 * domain - domain / 2
	end
	if (key_dezoom == 1) then
		domain = domain / ZOOM_COEF
	end
end

--------------------------------------------------
--[ DRAW ]----------------------------------------
--------------------------------------------------

function love.draw()
	draw_gpu()
	lg.setCanvas()
	lg.setBlendMode( 'alpha', 'premultiplied' )
	lg.draw( c_main, 0, 0, 0, WINDOW.scale, WINDOW.scale )
	lg.setBlendMode( 'alpha', 'alphamultiply' )
end

--------------------------------------------------
--[ EVENTS ]--------------------------------------
--------------------------------------------------

function love.mousemoved( x, y )
	mouse_x = x
	mouse_y = y
end

function love.keypressed( key )
	if ( key == 'escape' ) then le.quit() end
	if ( key == 'left' ) then key_l = 1 end
	if ( key == 'right' ) then key_r = 1 end
	if ( key == 'up' ) then key_u = 1 end
	if ( key == 'down' ) then key_d = 1 end
	if ( key == 'q' ) then key_dezoom = 1 end
	if ( key == 'w' ) then key_zoom = 1 end
	if ( key == 'space' ) then max_iter = max_iter + 5 end
end

function love.keyreleased( key )
	if ( key == 'left' ) then key_l = 0 end
	if ( key == 'right' ) then key_r = 0 end
	if ( key == 'up' ) then key_u = 0 end
	if ( key == 'down' ) then key_d = 0 end
	if ( key == 'q' ) then key_dezoom = 0 end
	if ( key == 'w' ) then key_zoom = 0 end
end
