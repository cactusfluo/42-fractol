--[[
--------------------------------------------------------------------------------
--	by REEZ
--	project name: Fractol
--	file name: conf.lua
--------------------------------------------------------------------------------
--	description:
--		Configuration file of Fractol.
--------------------------------------------------------------------------------
--]]

--------------------------------------------------------------------------------
--[ PROPERTIES ]----------------------------------------------------------------
--------------------------------------------------------------------------------

-- window table --
WINDOW =
{
	width = WINDOW_W,
	height = WINDOW_H,
	sizes =
	{
		{WINDOW_W * 1, WINDOW_H * 1},
		{WINDOW_W * 2, WINDOW_H * 2},
		{WINDOW_W * 3, WINDOW_H * 3},
		{WINDOW_W * 4, WINDOW_H * 4},
		{WINDOW_W * 5, WINDOW_H * 5}
	},
	scale = 1,
	vsync = true
}

--------------------------------------------------------------------------------
--[ CONF ]----------------------------------------------------------------------
--------------------------------------------------------------------------------

function love.conf(t)
	--[[ set game info ]]
	t.window.width = WINDOW.sizes[ WINDOW.scale ][ 1 ]
	t.window.height = WINDOW.sizes[ WINDOW.scale ][ 2 ]
	t.window.title = PROJECT_NAME
	t.window.vsync = WINDOW.vsync 

	--[[ disable unused modules ]]
	t.modules.physics = false
	t.modules.video = false
	t.modules.touch = false
end
