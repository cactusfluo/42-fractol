--[[
--------------------------------------------------------------------------------
--	by COLOR KIDS GAMES
--	game name: Otter Town
--	file name: shortcut.lua
--------------------------------------------------------------------------------
--	description:
--		This file make some shortcut on often used functions/modules for
--		efficiency
--------------------------------------------------------------------------------
--]]

------------
--- LOVE ---
------------
la	= love.audio
le	= love.event
lfs	= love.filesystem
lft	= love.font
lg	= love.graphics
li	= love.image
lj	= love.joystick
lk	= love.keyboard
lm	= love.math
ls	= love.sound
lw	= love.window
lm	= love.mouse

------------
--- MATH ---
------------
floor 	= math.floor
ceil	= math.ceil
abs		= math.abs
max		= math.max
min		= math.min
