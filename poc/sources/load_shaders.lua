--[[
--------------------------------------------------------------------------------
--	by COLOR KIDS GAMES
--	game name: Otter Town
--	file name: load_shaders.lua
--------------------------------------------------------------------------------
--	description:
--		Create every shaders of the game.
--------------------------------------------------------------------------------
--]]

local lg = lg

--------------------------------------------------------------------------------
--[ SHADERS ]-------------------------------------------------------------------
--------------------------------------------------------------------------------

sh_mandelbrot	= lg.newShader( 'sources/fractals/mandelbrot.glsl' )
sh_ship			= lg.newShader( 'sources/fractals/ship.glsl' )
sh_tunnel		= lg.newShader( 'sources/fractals/tunnel.glsl' )
