--[[
--------------------------------------------------------------------------------
--	by COLOR KIDS GAMES
--	game name: Otter Town
--	file name: lib.lua
--------------------------------------------------------------------------------
--	description:
--		Library for little functions.
--------------------------------------------------------------------------------
--]]

--------------------------------------------------------------------------------
--[ MODULE ]--------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------
--[ BODY ]----------------------------------------
--------------------------------------------------

local lib =
{
	fps =
	{
		fps = 0,
		delay = 1,
		list = {},
		i = 1
	}
}

--------------------------------------------------
--[ FUNCTIONS ]-----------------------------------
--------------------------------------------------

--[[
-- Set the fps displayer delay
--]]
function lib.fps.set_delay( delay )
	lib.fps.delay = delay
end

--[[
-- Update the fps
--]]
function lib.fps.update( dt )
	local delay = 60 * lib.fps.delay

	lib.fps.list[ lib.fps.i ] = 1 / dt
	lib.fps.i = lib.fps.i + 1
	if ( lib.fps.i > delay ) then
		local sum = 0
		lib.fps.i = 1
		for i=1, delay do
			sum = sum + lib.fps.list[ i ]
		end
		lib.fps.fps = floor( sum / delay )
	end
end

--[[
-- print the fps on the screen
--]]
function lib.fps.print()
	lg.setColor( 255, 255, 255, 127 )
	if ( lib.fps.fps <= 99 ) then
		lg.rectangle( 'fill', 0, 0, 37, 15 )
	else
		lg.rectangle( 'fill', 0, 0, 45, 15 )
	end
	lg.print( { { 0, 0, 0 }, lib.fps.fps .. 'fps' }, 1, 1 )
	lg.setColor( 255, 255, 255, 255 )
end

--------------------------------------------------
--[ RETURN ]--------------------------------------
--------------------------------------------------

return lib
