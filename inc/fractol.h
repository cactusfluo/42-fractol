/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:37:43 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 15:47:44 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

/*
********************************************************************************
** INCLUDES ********************************************************************
********************************************************************************
*/

# include <unistd.h>
# include <stdlib.h>
# include <stddef.h>
# include <stdint.h>
# include <pthread.h>
# include "mlx.h"
# include "full_libct.h"

/*
********************************************************************************
** DEFINES *********************************************************************
********************************************************************************
*/

# define WINDOW_W		700
# define WINDOW_H		700
# define ZOOM_COEF		0.98
# define MOVE_COEF		15
# define MIN_PRECISION	10

/*
********************************************************************************
** TYPEDEFS ********************************************************************
********************************************************************************
*/

typedef struct s_point		t_point;
typedef struct s_event		t_event;
typedef struct s_mlx		t_mlx;
typedef struct s_fractol	t_fractol;

/*
********************************************************************************
** STRUCTURES ******************************************************************
********************************************************************************
*/

struct			s_point
{
	uint16_t	screen_x : 10;
	uint16_t	screen_y : 10;
	double		x;
	double		y;
	double		zc;
	double		zr;
	uint32_t	level;
};

struct			s_mlx
{
	void		*mlx;
	void		*win;
	void		*mlx_canvas;
	uint8_t		*canvas;
	void		*mlx_target_canvas;
	uint8_t		*target_canvas;
};

struct			s_fractol
{
	t_mlx		mlx;
	uint16_t	mouse_x;
	uint16_t	mouse_y;
	double		pos_x;
	double		pos_y;
	double		domain;
	t_point		points[WINDOW_W * WINDOW_H];
	uint32_t	to_draw;
	uint32_t	level;
	uint32_t	(*f_init)();
	uint32_t	(*f_precise)();
	uint8_t		dynamic : 1;
};

/*
********************************************************************************
** FUNCTIONS *******************************************************************
********************************************************************************
*/

void			set_fractal(t_fractol *fct, uint8_t fractal);
int32_t			event_key(int32_t keycode, void *param);
int32_t			event_clic(int32_t button, int32_t x, int32_t y, void *param);
int32_t			event_mouse(int32_t x, int32_t y, void *param);
void			init_target(t_fractol *fct);
int32_t			loop(void *param);
void			draw_init(t_fractol *fct);
void			draw_precise(t_fractol *fct);
void			draw_target(t_fractol *fct);
uint32_t		calcul_init_mandelbrot(t_fractol *fct, t_point *p);
uint32_t		calcul_precise_mandelbrot(t_fractol *fct, t_point *p);
uint32_t		calcul_init_julia(t_fractol *fct, t_point *p);
uint32_t		calcul_precise_julia(t_fractol *fct, t_point *p);
uint32_t		calcul_init_ship(t_fractol *fct, t_point *p);
uint32_t		calcul_precise_ship(t_fractol *fct, t_point *p);
uint32_t		calcul_init_tunnel(t_fractol *fct, t_point *p);
uint32_t		calcul_precise_tunnel(t_fractol *fct, t_point *p);
int32_t			get_color(uint32_t level);

#endif
